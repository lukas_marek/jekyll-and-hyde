import numpy as np
from bitarray import bitarray
import jekyll.utilities as util


def LSBHide(image: np.array, messageBinary: bitarray) -> np.array:
    """ hides file/message to image via "least significant bit" method"""

    stegoImage = np.copy(image)
    index = 0
    for pixel in np.nditer(stegoImage, op_flags=['readwrite']):
        if(index < len(messageBinary)):
            # reads binary message and emplaces LSB
            if(messageBinary[index] == 1):
                pixel[...] = util.setBitPositionOne(pixel, 1)
            elif(messageBinary[index] == 0):
                pixel[...] = util.setBitPositionZero(pixel, 1)
            index += 1
        else:
            return stegoImage


def findLastPixel(differenceImage: np.array):
    """finds last pixel, so that file/message can be found"""

    i = -1
    lastPixel = list(np.array(np.where(differenceImage != 0))[::, i])
    while lastPixel[0] == differenceImage.shape[0] - 1 or lastPixel[1] == differenceImage.shape[1] - 1:
        lastPixel=list(np.array(np.where(differenceImage != 0))[::, i])
        i -= 1
    return lastPixel


def LSBFind(keyImage: np.array, stegoImage: np.array,) -> bitarray:
    """finds file/message hidden by LSB method"""

    differenceImage = stegoImage - keyImage

    lastPixel = findLastPixel(differenceImage)
    foundMessageBinary = bitarray()

    lastPixelReached = False
    it = np.nditer(stegoImage, flags=['multi_index'])
    for pixel in it:
        if lastPixelReached == True and len(foundMessageBinary) % 8 == 0:
            break
        foundMessageBinary.append(util.getLSBofNumber(pixel, 1))
        if list(it.multi_index) == lastPixel:
            lastPixelReached = True

    return foundMessageBinary


def LSBCheckCapacity(image: np.array, messageBinary: bitarray) -> bool:
    """checks capacity of image, if message is too big return False"""

    bitsAvaible = image.shape[0] * image.shape[1]
    if image.ndim == 3:
        bitsAvaible *= image.shape[2]
        bitsAvaible -= 3
    elif image.ndim == 2:
        bitsAvaible -= 1

    print("Message takes: ", end="")
    print(round((len(messageBinary) / bitsAvaible * 100), 2), end="")
    print("% of carrier image")

    if(bitsAvaible < len(messageBinary)):
        print("Error: file/message is too big")
        return False
    else:
        return True
