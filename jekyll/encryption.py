from cryptography.fernet import Fernet

#key = Fernet.generate_key()
key = b'2uieAC5vAiX2Q8Is2cGhcyrD1OtTzhFFsh9kYICChjk='


def encryptMessage(message: str) ->str:
    """encrypts message"""

    encryptedMessage = ""
    f = Fernet(key)
    encryptedMessage = f.encrypt(message.encode("utf-8"))

    return encryptedMessage.decode("utf-8")


def decryptMessage(encryptedMessage: str) -> str:
    """decrypts message"""
    
    f = Fernet(key)
    d = f.decrypt(encryptedMessage.encode("utf-8"))
    return d.decode("utf-8")


def encryptFile(file: str, outFile: str):
    """encrypts file"""
    
    f = Fernet(key)

    with open(file, "rb") as fil:
        decryptedFile = fil.read()

    encryptedFile = f.encrypt(decryptedFile)

    with open(outFile, "wb") as fil:
        fil.write(encryptedFile)


def decryptFile(file: str):
    """decrypts file"""
    
    f = Fernet(key)
    
    with open(file, "rb") as fil:
            encryptedFile = fil.read()
    
    decryptedFile = f.decrypt(encryptedFile)
    
    with open(file, "wb") as fil:
        fil.write(decryptedFile)