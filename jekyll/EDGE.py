from PIL import ImageFilter
from bitarray import bitarray
import numpy as np
import jekyll.utilities as util


def makeMask(keyImage: np.array) -> np.array:
    """creates mask image to determine places where information can be stored"""
    
    maskImage = np.copy(keyImage)
    maskImage = util.imageFromArray(maskImage)
    maskImage = maskImage.filter(ImageFilter.Kernel((3, 3), (1, 1, 1,
                                                             1, -8, 1,
                                                             1, 1, 1,), 1, 0))
    maskImage = np.array(maskImage, dtype='uint8')

    return maskImage


def EDGEHide(keyImage: np.array, maskImage: np.array, messageBinary: bitarray) -> np.array:
    """hides file/message to edges in an image"""

    stegoImage = np.copy(keyImage)

    index = 0
    it = np.nditer(stegoImage, op_flags=['readwrite'], flags=['multi_index'])
    for pixel in it:
        if maskImage[it.multi_index] > 110:
            if it.multi_index[0] != 0 and it.multi_index[0] != keyImage.shape[0] - 1:
                if it.multi_index[1] != 0 and it.multi_index[1] != keyImage.shape[1] - 1:
                    for i in range(1, 3):
                        if messageBinary[index] == 1:
                            pixel[...] = util.setBitPositionOne(pixel, i)
                        else:
                            pixel[...] = util.setBitPositionZero(pixel, i)
                        index += 1
                        if index >= len(messageBinary):
                            return stegoImage


def findLastPixel(differenceImage: np.array):
    """finds last pixel, so that file/message can be found"""
    
    i = -1
    lastPixel = list(np.array(np.where(differenceImage != 0))[::, i])
    while lastPixel[0] == differenceImage.shape[0] - 1 or lastPixel[1] == differenceImage.shape[1] - 1:
        lastPixel=list(np.array(np.where(differenceImage != 0))[::, i])
        i -= 1
    return lastPixel


def EDGEFind(keyImage: np.array, stegoImage: np.array, maskImage: np.array) -> np.array:
    """finds file/message hidden by EDGE method"""
    
    differenceImage = stegoImage - keyImage

    lastPixel = findLastPixel(differenceImage)
    foundMessageBinary = bitarray()

    lastPixelReached = False
    it = np.nditer(stegoImage, flags=['multi_index'])
    for pixel in it:
        if maskImage[it.multi_index] > 110:
            if it.multi_index[0] != 0 and it.multi_index[0] != keyImage.shape[0] - 1:
                if it.multi_index[1] != 0 and it.multi_index[1] != keyImage.shape[1] - 1:
                    for i in range(1, 3):
                        if lastPixelReached == True and len(foundMessageBinary) % 8 == 0:
                            break
                        foundMessageBinary.append(util.getLSBofNumber(pixel, i))
                        if list(it.multi_index) == lastPixel:
                            lastPixelReached = True

    return foundMessageBinary


def EDGEcheckCapacity(maskImage: np.array, messageBinary: bitarray):
    """checks capacity of image, if message is too big return False"""
    
    bitsAvaible = 0
    it = np.nditer(maskImage, op_flags=['readwrite'], flags=['multi_index'])
    for _ in it:
        if maskImage[it.multi_index] > 110:
            if it.multi_index[0] != 0 and it.multi_index[0] != maskImage.shape[0] - 1:
                if it.multi_index[1] != 0 and it.multi_index[1] != maskImage.shape[1] - 1:
                    bitsAvaible += 2
    if maskImage.ndim == 3:
        bitsAvaible -= 3
    elif maskImage.ndim == 2:
        bitsAvaible -= 1

    print("Message takes: ", end="")
    print(round(len(messageBinary)/bitsAvaible * 100, 2), end="")
    print("% of carrier image")

    if(bitsAvaible < len(messageBinary)):
        print("Error: file/message is too big")
        return False
    else:
        return True