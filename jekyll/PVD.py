import numpy as np
from bitarray import bitarray
from bitarray.util import ba2int
from bitarray.util import int2ba
import math
from typing import Tuple


def pixelDifference(p1: int, p2: int) -> int:
    """returns absolute difference of two pixels"""
    
    pixel1 = p1.item()
    pixel2 = p2.item()
    return pixel2 - pixel1


def findBounds(ranges: np.array, difference: int) -> Tuple[int, int]:
    """determines to which set of bounds the given difference belongs to
    necessary for future determinization of how much information can be
    stored in two pixels"""
    
    if difference < 0:
        difference = -difference
    for l, u in ranges:
        if difference >= l and difference <= u:
            return (l, u)


def numberOfBits(l: int, u: int) -> int:
    """returns number of bits that can be stored in pair of pixels"""

    return int(math.log2(u - l + 1))


def findNewDifference(l: int, bits: bitarray, d: int) -> int:
    """determines new difference between pixels"""
    
    if d >= 0:
        return l + ba2int(bits)
    else:
        return -(l + ba2int(bits))


def newPixelValues(p1: int, p2: int, newDifference: int, difference: int) -> Tuple[int, int]:
    """gives pixels new values depending on the new difference between them"""
    
    m = newDifference - difference
    ceiling = math.ceil(m/2)
    floor = math.floor(m/2)
    if difference % 2 != 0:
        if p1 - ceiling < 0 or p1 - ceiling > 255 or p2 + floor < 0 or p2 + floor > 255:
            return (0, 0, False)
        else:
            return (np.uint8(p1 - ceiling), np.uint8(p2 + floor), True)

    else:
        if p1 - floor < 0 or p1 - floor > 255 or p2 + ceiling < 0 or p2 + ceiling > 255:
            return (0, 0, False)
        else:
            return (np.uint8(p1 - floor), np.uint8(p2 + ceiling), True)


def PVDHide(image: np.array, messageBinary: bitarray):
    """pixel value differencing method of hiding information,
    if message is too big return False"""
    
    ranges = np.array(((0, 7),
                       (8, 15),
                       (16, 31),
                       (32, 63),
                       (64, 127),
                       (128, 255)))

    stegoImage = np.copy(image)

    messageBinary.extend("10000000000000000000000000000000")

    index = 0
    stop = False
    if image.ndim == 3:
        for c in range(0, stegoImage.shape[2]):
            for y in range(0, stegoImage.shape[0]):
                for x in range(0, stegoImage.shape[1], 2):
                    if y >= stegoImage.shape[0] - 1 and x >= stegoImage.shape[1] - 4 and c >= stegoImage.shape[2] - 1:
                        print("Message takes: ", end="")
                        print(round(len(messageBinary)/index * 100, 2), end="")
                        print("% of carrier image")
                        print("Error: file/message is too big")
                        return False
                    if x == stegoImage.shape[1] - 1:
                        continue
                    p1 = stegoImage[y, x, c]
                    p2 = stegoImage[y, x + 1, c]
                    difference = pixelDifference(p1, p2)
                    l, u = findBounds(ranges, difference)
                    n = numberOfBits(l, u)
                    indexEnd = index + n
                    if indexEnd >= len(messageBinary):
                        indexEnd = len(messageBinary)
                        stop = True
                    bits = messageBinary[index: indexEnd]
                    newDifference = findNewDifference(l, bits, difference)
                    _, _, check = newPixelValues(p1, p2, u, difference)
                    if check == False:
                        continue
                    #print(p1, p2, ":", difference, "d", "-", ba2int(bits), "b", "->", newDifference, "nd")
                    # print(bits)
                    p1, p2, _ = newPixelValues(
                        p1, p2, newDifference, difference)
                    #print(p1, p2)
                    index += n
                    stegoImage[y, x, c] = p1
                    stegoImage[y, x + 1, c] = p2
                    if stop == True:
                        return stegoImage
    elif image.ndim == 2:
        for y in range(0, stegoImage.shape[0]):
            for x in range(0, stegoImage.shape[1], 2):
                if x >= stegoImage.shape[1] - 2 and y >= stegoImage.shape[0] - 1:
                    return False
                if x == stegoImage.shape[1] - 1:
                    continue
                p1 = stegoImage[y, x]
                p2 = stegoImage[y, x + 1]
                difference = pixelDifference(p1, p2)
                l, u = findBounds(ranges, difference)
                n = numberOfBits(l, u)
                indexEnd = index + n
                if indexEnd >= len(messageBinary):
                    indexEnd = len(messageBinary)
                    stop = True
                bits = messageBinary[index: indexEnd]
                newDifference = findNewDifference(l, bits, difference)
                _, _, check = newPixelValues(p1, p2, u, difference)
                if check == False:
                    continue
                #print(p1, p2, ":", difference, "d", "-", ba2int(bits), "b", "->", newDifference, "nd")
                # print(bits)
                p1, p2, _ = newPixelValues(p1, p2, newDifference, difference)
                #print(p1, p2)
                index += n
                stegoImage[y, x] = p1
                stegoImage[y, x + 1] = p2
                if stop == True:
                    return stegoImage


def findLastPixel(differenceImage: np.array):
    """finds last pixel, so that file/message can be found"""
    
    if differenceImage.ndim == 3:
        maxc = 0
        maxx = 0
        maxy = 0

        # find color
        stop = False
        for y in range(0, differenceImage.shape[0]):
            if stop == True:
                break
            if y == 1:
                break
            for x in range(0, differenceImage.shape[1], 2):
                if stop == True:
                    break
                for c in range(0, differenceImage.shape[2]):
                    if differenceImage[y, x, c] != 0:
                        if c > maxc:
                            maxc = c
                    if maxc == 2:
                        stop = True
        # find y
        nowhereToGo = False
        for y in range(0, differenceImage.shape[0]):
            if nowhereToGo == True:
                break
            for x in range(0, differenceImage.shape[1]):
                if nowhereToGo == True:
                    break
                if differenceImage[y, x, maxc] != 0:
                    maxy = y
                    break
                if x == differenceImage.shape[1] - 1 and y > maxy:
                    nowhereToGo = True

        # find x
        for x in range(0, differenceImage.shape[1]):
            if differenceImage[maxy, x, maxc] != 0 and x > maxx:
                if maxy == differenceImage.shape[0] - 1 and x == differenceImage.shape[1] - 1:
                    break
                maxx = x

        return maxy, maxx, maxc

    elif differenceImage.ndim == 2:
        i = -1
        lastPixel = list(np.array(np.where(differenceImage != 0))[::, i])
        while lastPixel[0] == differenceImage.shape[0] - 1 or lastPixel[1] == differenceImage.shape[1] - 1:
            lastPixel=list(np.array(np.where(differenceImage != 0))[::, i])
            i -= 1
        return lastPixel


def PVDFind(keyImage: np.array, stegoImage: np.array, differenceImage: np.array) -> bitarray:
    """finds file/message hidden by PVD method"""
    
    ranges = np.array(((0, 7),
                       (8, 15),
                       (16, 31),
                       (32, 63),
                       (64, 127),
                       (128, 255)))

    maxy, maxx, maxc = findLastPixel(differenceImage)
    #print(maxy, maxx, maxc)

    foundMessageBinary = bitarray()

    if keyImage.ndim == 3:
        for c in range(0, stegoImage.shape[2]):
            for y in range(0, stegoImage.shape[0]):
                for x in range(0, stegoImage.shape[1], 2):
                    if x == stegoImage.shape[1] - 1:
                        continue
                    p1 = keyImage[y, x, c]
                    p2 = keyImage[y, x + 1, c]
                    difference = pixelDifference(p1, p2)
                    l, u = findBounds(ranges, difference)
                    _, _, check = newPixelValues(p1, p2, u, difference)
                    if check == False:
                        continue
                    ps1 = stegoImage[y, x, c]
                    ps2 = stegoImage[y, x + 1, c]
                    newDifference = abs(pixelDifference(ps1, ps2))
                    b = newDifference - l
                    n = numberOfBits(l, u)
                    bits = int2ba(int(b))
                    prefixBits = bitarray()
                    #print(n, len(bits))
                    for _ in range(0, n - len(bits)):
                        prefixBits.append(0)
                    prefixBits.extend(bits)
                    #print(p1, p2,":", difference, "d","-", b, "b", "->", newDifference, "nd")
                    # print(prefixBits)
                    foundMessageBinary.extend(prefixBits)

                    zeroCount = 0
                    if y >= maxy and x >= maxx and c >= maxc:
                        while True:
                            if zeroCount == 16:
                                while foundMessageBinary[len(foundMessageBinary) - 1] != 1:
                                    foundMessageBinary.pop()
                                foundMessageBinary.pop()
                                return foundMessageBinary
                            if foundMessageBinary[len(foundMessageBinary) - 1] == 0:
                                zeroCount += 1
                            else:
                                zeroCount = 0
                            foundMessageBinary.pop()

    elif keyImage.ndim == 2:
        for y in range(0, stegoImage.shape[0]):
            for x in range(0, stegoImage.shape[1], 2):
                if x == stegoImage.shape[1] - 1:
                    continue
                p1 = keyImage[y, x]
                p2 = keyImage[y, x + 1]
                difference = pixelDifference(p1, p2)
                l, u = findBounds(ranges, difference)
                _, _, check = newPixelValues(p1, p2, u, difference)
                if check == False:
                    continue
                ps1 = stegoImage[y, x]
                ps2 = stegoImage[y, x + 1]
                newDifference = abs(pixelDifference(ps1, ps2))
                b = newDifference - l
                n = numberOfBits(l, u)
                bits = int2ba(int(b))
                prefixBits = bitarray()
                #print(n, len(bits))
                for _ in range(0, n - len(bits)):
                    prefixBits.append(0)
                prefixBits.extend(bits)
                #print(p1, p2,":", difference, "d","-", b, "b", "->", newDifference, "nd")
                # print(prefixBits)
                foundMessageBinary.extend(prefixBits)

                zeroCount = 0
                if y >= maxy and x >= maxx:
                    while True:
                        if zeroCount == 16:
                            while foundMessageBinary[len(foundMessageBinary) - 1] != 1:
                                foundMessageBinary.pop()
                            foundMessageBinary.pop()
                            return foundMessageBinary
                        if foundMessageBinary[len(foundMessageBinary) - 1] == 0:
                            zeroCount += 1
                        else:
                            zeroCount = 0
                        foundMessageBinary.pop()


def PVDCheckCapacity(differenceImage: np.array):
    """computes capacity of an image"""
    
    if differenceImage.ndim == 3:
        y, x, c = findLastPixel(differenceImage)
        filled = 0
        filled = y * differenceImage.shape[1]
        filled += x
        filled += c * differenceImage.shape[0] * differenceImage.shape[1]
        bitsAvaible = differenceImage.shape[0] * differenceImage.shape[1]
        bitsAvaible *= differenceImage.shape[2]
        bitsAvaible -= 6
        print("Message takes: ", end="")
        print(round(filled/bitsAvaible * 100, 2), end="")
        print("% of carrier image")
    elif differenceImage.ndim == 2:
        y, x = findLastPixel(differenceImage)
        filled = 0
        if y >= 1:
            filled = y * differenceImage.shape[1]
        filled += x
        bitsAvaible = differenceImage.shape[0] * differenceImage.shape[1]
        bitsAvaible -= 2
        print("Message takes: ", end="")
        print(round(filled/bitsAvaible * 100, 2), end="")
        print("% of carrier image")
