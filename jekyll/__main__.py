import jekyll.LSB as lsb
import jekyll.PVD as pvd
import jekyll.EDGE as edge
import jekyll.utilities as util
import jekyll.encryption as crypt
import numpy as np
import os
import shutil
import click


def setMark(stegoImage: np.array, number: int) -> np.array:
    """marks stego image with information whether file or messege has been
    hidden and what algorithm has been used to do so"""
    
    if stegoImage.ndim == 3:
        maxy = stegoImage.shape[0] - 1
        maxx = stegoImage.shape[1] - 1
        color = 2
        if stegoImage[maxy, maxx, color] - number >= 0:
            stegoImage[maxy, maxx, color] -= number
            return stegoImage
        else:
            stegoImage[maxy, maxx, color] += number
            return stegoImage
    elif stegoImage.ndim == 2:
        maxy = stegoImage.shape[0] - 1
        maxx = stegoImage.shape[1] - 1
        if stegoImage[maxy, maxx] - number >= 0:
            stegoImage[maxy, maxx] -= number
            return stegoImage
        else:
            stegoImage[maxy, maxx] += number
            return stegoImage

def getmark(keyImage: np.array, stegoImage: np.array) -> int:
    """retrieves mark from stego image thats holding information about
    whether file or messege has been hidden and what algorithm
    has been used to do so"""

    if stegoImage.ndim == 3:
        maxy = stegoImage.shape[0] - 1
        maxx = stegoImage.shape[1] - 1
        color = 2
        pixel1 = stegoImage[maxy, maxx, color].item()
        pixel2 = keyImage[maxy, maxx, color].item()
        return abs(pixel1 - pixel2)

    elif stegoImage.ndim == 2:
        maxy = stegoImage.shape[0] - 1
        maxx = stegoImage.shape[1] - 1
        pixel1 = stegoImage[maxy, maxx].item()
        pixel2 = keyImage[maxy, maxx].item()
        return abs(pixel1 - pixel2)


#LSB
def hydeLSBMessage(encrypted, message, inputImage, outputImage):
    """hides message using LSB algo"""
    
    keyImage = util.loadImage(inputImage)

    if encrypted == True:
        message = crypt.encryptMessage(message)

    messageBinary = util.messageToBinary(message)

    if lsb.LSBCheckCapacity(keyImage, messageBinary) == False:
        return

    stegoImage = lsb.LSBHide(keyImage, messageBinary)

    stegoImage = setMark(stegoImage, 0)

    util.computeStatistics(keyImage, stegoImage)

    util.imageFromArray(stegoImage).save(outputImage)

def jekyllLSBMessage(keyImage, stegoImage, encrypted):
    """finds message using LSB algo"""
    
    foundMessageBinary = lsb.LSBFind(keyImage, stegoImage)

    foundMessage = util.binaryToMessage(foundMessageBinary)

    if encrypted == True:
        foundMessage = crypt.decryptMessage(foundMessage)

    print(foundMessage)


def hydeLSBFile(encrypted, file, inputImage, outputImage):
    """hides file using LSB algo"""
    
    keyImage = util.loadImage(inputImage)

    encryptedFile = ""
    if encrypted == True:
        shutil.copyfile(file, "enc")
        encryptedFile = "enc"
        crypt.encryptFile(file, encryptedFile)
        file = encryptedFile

    fileBinary = util.fileToBinary(file)

    if encrypted == True:
        os.remove(encryptedFile)

    if lsb.LSBCheckCapacity(keyImage, fileBinary) == False:
        return

    stegoImage = lsb.LSBHide(keyImage, fileBinary)

    stegoImage = setMark(stegoImage, 1)

    util.computeStatistics(keyImage, stegoImage)

    util.imageFromArray(stegoImage).save(outputImage)

def jekyllLSBFile(keyImage, stegoImage, encrypted):
    """finds file using LSB algo"""
    
    foundFileBinary = lsb.LSBFind(keyImage, stegoImage)

    util.binaryToFile(foundFileBinary, "foundFile")

    if encrypted == True:
        crypt.decryptFile("foundFile")

    util.addExtension("foundFile")

#PVD
def hydePVDMessage(encrypted, message, inputImage, outputImage):
    """hides message using PVD algo"""
    
    keyImage = util.loadImage(inputImage)

    if encrypted == True:
        message = crypt.encryptMessage(message)

    messageBinary = util.messageToBinary(message)

    stegoImage = pvd.PVDHide(keyImage, messageBinary)

    if isinstance(stegoImage, bool) == True:
        return

    differenceImage = stegoImage - keyImage

    pvd.PVDCheckCapacity(differenceImage)

    stegoImage = setMark(stegoImage, 2)

    util.computeStatistics(keyImage, stegoImage)

    util.imageFromArray(stegoImage).save(outputImage)

def jekyllPVDMessage(keyImage, stegoImage, encrypted):
    """finds message using PVD algo"""

    differenceImage = keyImage - stegoImage

    foundMessageBinary = pvd.PVDFind(keyImage, stegoImage, differenceImage)

    foundMessage = util.binaryToMessage(foundMessageBinary)

    if encrypted == True:
        foundMessage = crypt.decryptMessage(foundMessage)

    print(foundMessage)


def hydePVDFile(encrypted, file, inputImage, outputImage):
    """hides file using PVD algo"""
    
    keyImage = util.loadImage(inputImage)

    encryptedFile = ""
    if encrypted == True:
        shutil.copyfile(file, "enc")
        encryptedFile = "enc"
        crypt.encryptFile(file, encryptedFile)
        file = encryptedFile

    fileBinary = util.fileToBinary(file)

    if encrypted == True:
        os.remove(encryptedFile)

    stegoImage = pvd.PVDHide(keyImage, fileBinary)

    if isinstance(stegoImage, bool) == True:
        return

    differenceImage = stegoImage - keyImage

    pvd.PVDCheckCapacity(differenceImage)

    stegoImage = setMark(stegoImage, 3)

    util.computeStatistics(keyImage, stegoImage)

    util.imageFromArray(stegoImage).save(outputImage)

def jekyllPVDFile(keyImage, stegoImage, encrypted):
    """finds file using PVD algo"""
    
    differenceImage = keyImage - stegoImage
    foundFileBinary = pvd.PVDFind(keyImage, stegoImage, differenceImage)

    util.binaryToFile(foundFileBinary, "foundFile")

    if encrypted == True:
        crypt.decryptFile("foundFile")

    util.addExtension("foundFile")

#EDGE
def hydeEDGEMessage(encrypted, message, inputImage, outputImage):
    """hides message using EDGE algo"""
    
    keyImage = util.loadImage(inputImage)
    maskImage = edge.makeMask(keyImage)

    if encrypted == True:
        message = crypt.encryptMessage(message)

    messageBinary = util.messageToBinary(message)

    if edge.EDGEcheckCapacity(maskImage, messageBinary) == False:
        return

    stegoImage = edge.EDGEHide(keyImage, maskImage, messageBinary)

    stegoImage = setMark(stegoImage, 4)

    util.computeStatistics(keyImage, stegoImage)

    util.imageFromArray(stegoImage).save(outputImage)

def jekyllEDGEMessage(keyImage, stegoImage, encrypted):
    """finds message using EDGE algo"""

    maskImage = edge.makeMask(keyImage)
    foundMessageBinary = edge.EDGEFind(keyImage, stegoImage, maskImage)

    foundMessage = util.binaryToMessage(foundMessageBinary)

    if encrypted == True:
        foundMessage = crypt.decryptMessage(foundMessage)

    print(foundMessage)


def hydeEDGEFile(encrypted, file, inputImage, outputImage):
    """hides file using EDGE algo"""
    
    keyImage = util.loadImage(inputImage)
    maskImage = edge.makeMask(keyImage)

    encryptedFile = ""
    if encrypted == True:
        shutil.copyfile(file, "enc")
        encryptedFile = "enc"
        crypt.encryptFile(file, encryptedFile)
        file = encryptedFile

    fileBinary = util.fileToBinary(file)

    if encrypted == True:
        os.remove(encryptedFile)

    if edge.EDGEcheckCapacity(maskImage, fileBinary) == False:
        return

    stegoImage = edge.EDGEHide(keyImage, maskImage, fileBinary)

    stegoImage = setMark(stegoImage, 5)

    util.computeStatistics(keyImage, stegoImage)

    util.imageFromArray(stegoImage).save(outputImage)

def jekyllEDGEFile(keyImage, stegoImage, encrypted):
    """finds file using EDGE algo"""
    
    maskImage = edge.makeMask(keyImage)
    foundFileBinary = edge.EDGEFind(keyImage, stegoImage, maskImage)

    util.binaryToFile(foundFileBinary, "foundFile")

    if encrypted == True:
        crypt.decryptFile("foundFile")

    util.addExtension("foundFile")


#Dr Jekyll and Mr. Hyde 
def hydeErrors(algo, message, file, images) -> bool:
    if len(algo) == 0:
        print("Error: Algorithm has to be chosen")
        print('\nfor more info please visit "hyde --help"')
        return False

    algos = {"LSB", "PVD", "EDGE"}
    if algo.upper() not in algos:
        print("Error: Please choose from one of the folowing alghorithms: LSB, PVD, EDGE")
        print('\nfor more info please visit "hyde --help"')
        return False

    if len(message) == 0 and len(file) == 0:
        print("Error: Either message or file has to be passed")
        print('\nfor more info please visit "hyde --help"')
        return False

    if len(message) != 0 and len(file) != 0:
        print("Error: Both message and file cannot be passed")
        print('\nfor more info please visit "hyde --help"')
        return False

    if len(images) != 2:
        print("Error: Exactly two images must be chosen: key image and output stego image")
        print('\nfor more info please visit "hyde --help"')
        return False


@click.command()
@click.option("--algo", "-a", default="", type=str, help="What algorith to use. You can choose from LSB, PVD and EDGE. LSB is the fastest one, PVD has the biggest capacity and is a bit smarter than LSB as it determines better where to hide how much information and EDGE is almost undetectable but with low capacity, not really suitable for hiding files")
@click.option("--encrypted", "-e", is_flag=True, help="Wheter or not you'd like to encrypt hidden file/message")
@click.option("--message", "-m", default="", help="Wheter or not you'd like to hide a message, name must be provided afterwards. File can't be hidden at the same time.")
@click.option("--file", "-f", default="", help="Wheter or not you'd like to hide a file, name must be provided afterwards. Message can't be hidden at the same time")
@click.argument("images", nargs=2, type=str)
def hyde(algo, encrypted, message, file, images):
    """This function hides a file/message into provided carrier image.
     At the end exactly two images must be provided, carrier image and output stego image"""

    if hydeErrors(algo, message, file, images) == False:
        return

    if len(message) != 0:
        if algo.upper() == "LSB":
            hydeLSBMessage(encrypted, message, images[0], images[1])

        if algo.upper() == "PVD":
            hydePVDMessage(encrypted, message, images[0], images[1])

        if algo.upper() == "EDGE":
            hydeEDGEMessage(encrypted, message, images[0], images[1])

    else:
        if algo.upper() == "LSB":
            hydeLSBFile(encrypted, file, images[0], images[1])

        if algo.upper() == "PVD":
            hydePVDFile(encrypted, file, images[0], images[1])

        if algo.upper() == "EDGE":
            hydeEDGEFile(encrypted, file, images[0], images[1])


@click.command()
@click.option("--encrypted", "-e", is_flag=True, help="Wheter or not you'd like to decrypt hidden file/message")
@click.argument("images", nargs=-1, type=str)
def jekyll(images, encrypted):
    """Finds a file/message inside a stego image.
    At the end exactly two images must be provided, original key image and stego image"""

    if len(images) != 2:
        print("Error: Exactly two images must be provided: key image and stego image")
        print('\nfor more info please visit "jekyll --help"')
        return False

    keyImage = util.loadImage(images[0])
    stegoImage = util.loadImage(images[1])

    mark = getmark(keyImage, stegoImage)

    if mark == 0:
        jekyllLSBMessage(keyImage, stegoImage, encrypted)

    elif mark == 1:
        jekyllLSBFile(keyImage, stegoImage, encrypted)

    elif mark == 2:
        jekyllPVDMessage(keyImage, stegoImage, encrypted)

    elif mark == 3:
        jekyllPVDFile(keyImage, stegoImage, encrypted)

    elif mark == 4:
        jekyllEDGEMessage(keyImage, stegoImage, encrypted)

    elif mark == 5:
        jekyllEDGEFile(keyImage, stegoImage, encrypted)

