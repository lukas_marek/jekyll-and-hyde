import numpy as np
from PIL import Image
import math
from bitarray import bitarray
from bitarray.util import ba2int
import fleep
import os

def addExtension(file: str):
    """adds extension to a found file"""

    oldFile = file
    with open(file, "rb") as file:
        info = fleep.get(file.read(128))

    newFile = oldFile + "." + info.extension[0]
    os.rename(oldFile, newFile)
    return info.extension[0]


def setBitPositionOne(number: int, k: int):
    """sets bit to 1 at a given position"""

    return number | k

def setBitPositionZero(number: int, k: int):
    """sets bit to 0 at a given position"""

    return number & ~k

def getLSBofNumber(number: int, k: int) -> bool:
    """returns bit at a given position"""
    
    return number & k


def messageToBinary(message: str) -> bitarray:
    """converts string to binary"""
    
    messageBinary = bitarray()
    messageBinary.frombytes(message.encode('utf-8'))

    return messageBinary

def binaryToMessage(messageBinary: bitarray) -> str:
    """converts binary to string"""

    return messageBinary.tobytes().decode('utf-8')

def fileToBinary(path: str) -> bitarray:
    """converts file to binary"""
    
    binary = bitarray()
    with open(path, 'rb') as f:
        binary.fromfile(f)

    return binary

def binaryToFile(binary: bitarray, path: str):
    """converts binary to file"""

    with open(path, 'wb') as f:
        binary.tofile(f)


def loadImage(imageName: str) -> np.array:
    """loads image to np.array"""

    im = Image.open(imageName)
    image = np.array(im, dtype='uint8')

    return image

def imageFromArray(image: np.array):
    """creates PIL image from np array"""

    PILImage = Image.fromarray(image, 'RGB')
    return PILImage


def meanSquareError(originalImage: np.array, encryptedImage: np.array) -> float:
    """determines mean square error of images"""
    
    return ((originalImage-encryptedImage)**2).mean()

def pixelSignalToNoiseRation(mse: float) -> float:
    """determines pixel signal to noise ratio"""
    
    if math.isclose(mse, 0):
        return 100

    return 10 * math.log10((255 - 1)**2 / mse)

def computeStatistics(originalImage: np.array, encryptedImage: np.array):
    """shows statistics of stego image"""
    
    print("Mean square error:", end=" ")
    mse = meanSquareError(originalImage, encryptedImage)
    print(mse)
    print("Peak signal to noise ratio:", end=" ")
    print(pixelSignalToNoiseRation(mse))
