import jekyll.LSB as lsb
import jekyll.PVD as pvd
import jekyll.EDGE as edge
import jekyll.utilities as util
import jekyll.encryption as crypt
import jekyll.__main__ as main
import numpy as np
import pytest
import random
import string
import os
import shutil


keyImage = util.loadImage("images/towers.jpg")

# binary
@pytest.fixture
def randomString():
    word = ""
    for _ in range(0, random.randint(1, 10000)):
        word = word + random.choice(string.ascii_lowercase)
    print(word)
    return word

@pytest.mark.parametrize('times', range(0, 10))
def test_binary_translation(randomString, times):
    message = randomString
    messageBinary = util.messageToBinary(message)
    messageTranslated = util.binaryToMessage(messageBinary)
    assert(message == messageTranslated)


# capacity check
def test_LSB_capacity_full():
    message = "a" * 999999999
    messageBinary = util.messageToBinary(message)
    assert(not lsb.LSBCheckCapacity(keyImage, messageBinary))

def test_PVD_capacity_full():
    message = "a" * 999999999
    messageBinary = util.messageToBinary(message)
    assert(not pvd.PVDHide(keyImage, messageBinary))

def test_EDGE_capacity_full():
    message = "a" * 999999999
    messageBinary = util.messageToBinary(message)
    maskImage = edge.makeMask(keyImage)
    assert(not edge.EDGEcheckCapacity(maskImage, messageBinary))

def test_LSB_capacity():
    message = "a"
    messageBinary = util.messageToBinary(message)
    assert(lsb.LSBCheckCapacity(keyImage, messageBinary))

def test_PVD_capacity():
    message = "a"
    messageBinary = util.messageToBinary(message)
    assert(not isinstance(pvd.PVDHide(keyImage, messageBinary), bool))

def test_EDGE_capacity():
    message = "a"
    messageBinary = util.messageToBinary(message)
    maskImage = edge.makeMask(keyImage)
    assert(edge.EDGEcheckCapacity(maskImage, messageBinary))


# encryption
@pytest.mark.parametrize('times', range(0, 10))
def test_encryption(randomString, times):
    message = randomString
    messageEncrypted = crypt.encryptMessage(message)
    messageDecrypted = crypt.decryptMessage(messageEncrypted)
    assert(message == messageDecrypted)


#steganography
def test_LSB_message_encrypted(randomString):
    encrypted = True

    originalMessage = randomString
    message = originalMessage

    if encrypted == True:
        message = crypt.encryptMessage(message)

    messageBinary = util.messageToBinary(message)

    if lsb.LSBCheckCapacity(keyImage, messageBinary) == False:
        return

    stegoImage = lsb.LSBHide(keyImage, messageBinary)

    stegoImage = main.setMark(stegoImage, 0)

    util.computeStatistics(keyImage, stegoImage)

    util.imageFromArray(stegoImage).save("output.png")

    # jekyll
    stegoImage = util.loadImage("output.png")
    foundMessageBinary = lsb.LSBFind(keyImage, stegoImage)

    foundMessage = util.binaryToMessage(foundMessageBinary)

    if encrypted == True:
        foundMessage = crypt.decryptMessage(foundMessage)

    os.remove("output.png")

    assert(foundMessage == originalMessage)

def test_LSB_message(randomString):
    encrypted = False

    originalMessage = randomString
    message = originalMessage

    if encrypted == True:
        message = crypt.encryptMessage(message)

    messageBinary = util.messageToBinary(message)

    if lsb.LSBCheckCapacity(keyImage, messageBinary) == False:
        return

    stegoImage = lsb.LSBHide(keyImage, messageBinary)

    stegoImage = main.setMark(stegoImage, 0)

    util.computeStatistics(keyImage, stegoImage)

    util.imageFromArray(stegoImage).save("output.png")

    # jekyll
    stegoImage = util.loadImage("output.png")
    foundMessageBinary = lsb.LSBFind(keyImage, stegoImage)

    foundMessage = util.binaryToMessage(foundMessageBinary)

    if encrypted == True:
        foundMessage = crypt.decryptMessage(foundMessage)

    os.remove("output.png")

    assert(foundMessage == originalMessage)

def test_LSB_file_encrypted():
    encrypted = True
    file = "images/towers_small.jpg"
    outputImage = "output.png"

    encryptedFile = ""
    if encrypted == True:
        shutil.copyfile(file, "enc")
        encryptedFile = "enc"
        crypt.encryptFile(file, encryptedFile)
        file = encryptedFile

    fileBinary = util.fileToBinary(file)

    if encrypted == True:
        os.remove(encryptedFile)

    if lsb.LSBCheckCapacity(keyImage, fileBinary) == False:
        return

    stegoImage = lsb.LSBHide(keyImage, fileBinary)

    stegoImage = main.setMark(stegoImage, 1)

    util.imageFromArray(stegoImage).save(outputImage)

    # jekyll
    stegoImage = util.loadImage("output.png")

    foundFileBinary = lsb.LSBFind(keyImage, stegoImage)

    util.binaryToFile(foundFileBinary, "foundFile")

    if encrypted == True:
        crypt.decryptFile("foundFile")

    extension = util.addExtension("foundFile")

    originalFile = util.loadImage("images/towers_small.jpg")
    foundFile = util.loadImage("foundFile." + extension)
    os.remove("foundFile." + extension)
    os.remove(outputImage)
    assert(np.array_equal(originalFile, foundFile))

def test_LSB_file():
    encrypted = False
    file = "images/towers_small.jpg"
    outputImage = "output.png"

    encryptedFile = ""
    if encrypted == True:
        shutil.copyfile(file, "enc")
        encryptedFile = "enc"
        crypt.encryptFile(file, encryptedFile)
        file = encryptedFile

    fileBinary = util.fileToBinary(file)

    if encrypted == True:
        os.remove(encryptedFile)

    if lsb.LSBCheckCapacity(keyImage, fileBinary) == False:
        return

    stegoImage = lsb.LSBHide(keyImage, fileBinary)

    stegoImage = main.setMark(stegoImage, 1)

    util.imageFromArray(stegoImage).save(outputImage)

    # jekyll
    stegoImage = util.loadImage("output.png")

    foundFileBinary = lsb.LSBFind(keyImage, stegoImage)

    util.binaryToFile(foundFileBinary, "foundFile")

    if encrypted == True:
        crypt.decryptFile("foundFile")

    extension = util.addExtension("foundFile")

    originalFile = util.loadImage("images/towers_small.jpg")
    foundFile = util.loadImage("foundFile." + extension)
    os.remove("foundFile." + extension)
    os.remove(outputImage)
    assert(np.array_equal(originalFile, foundFile))


def test_PVD_message_encrypted(randomString):
    encrypted = True

    originalMessage = randomString
    message = originalMessage

    if encrypted == True:
        message = crypt.encryptMessage(message)

    messageBinary = util.messageToBinary(message)

    stegoImage = pvd.PVDHide(keyImage, messageBinary)

    stegoImage = main.setMark(stegoImage, 0)

    util.computeStatistics(keyImage, stegoImage)

    util.imageFromArray(stegoImage).save("output.png")

    # jekyll
    stegoImage = util.loadImage("output.png")
    foundMessageBinary = pvd.PVDFind(keyImage, stegoImage, keyImage-stegoImage)

    foundMessage = util.binaryToMessage(foundMessageBinary)

    if encrypted == True:
        foundMessage = crypt.decryptMessage(foundMessage)

    os.remove("output.png")

    assert(foundMessage == originalMessage)

def test_PVD_message(randomString):
    encrypted = False

    originalMessage = randomString
    message = originalMessage

    if encrypted == True:
        message = crypt.encryptMessage(message)

    messageBinary = util.messageToBinary(message)

    stegoImage = pvd.PVDHide(keyImage, messageBinary)

    stegoImage = main.setMark(stegoImage, 0)

    util.computeStatistics(keyImage, stegoImage)

    util.imageFromArray(stegoImage).save("output.png")

    # jekyll
    stegoImage = util.loadImage("output.png")
    foundMessageBinary = pvd.PVDFind(keyImage, stegoImage, keyImage-stegoImage)

    foundMessage = util.binaryToMessage(foundMessageBinary)

    if encrypted == True:
        foundMessage = crypt.decryptMessage(foundMessage)

    os.remove("output.png")

    assert(foundMessage == originalMessage)

def test_PVD_file_encrypted():
    encrypted = True
    file = "images/towers_small.jpg"
    outputImage = "output.png"

    encryptedFile = ""
    if encrypted == True:
        shutil.copyfile(file, "enc")
        encryptedFile = "enc"
        crypt.encryptFile(file, encryptedFile)
        file = encryptedFile

    fileBinary = util.fileToBinary(file)

    if encrypted == True:
        os.remove(encryptedFile)

    stegoImage = pvd.PVDHide(keyImage, fileBinary)

    stegoImage = main.setMark(stegoImage, 1)

    util.imageFromArray(stegoImage).save(outputImage)

    # jekyll
    stegoImage = util.loadImage("output.png")

    foundFileBinary = pvd.PVDFind(keyImage, stegoImage, keyImage - stegoImage)

    util.binaryToFile(foundFileBinary, "foundFile")

    if encrypted == True:
        crypt.decryptFile("foundFile")

    extension = util.addExtension("foundFile")

    originalFile = util.loadImage("images/towers_small.jpg")
    foundFile = util.loadImage("foundFile." + extension)
    os.remove("foundFile." + extension)
    os.remove(outputImage)
    assert(np.array_equal(originalFile, foundFile))

def test_PVD_file():
    encrypted = False
    file = "images/towers_small.jpg"
    outputImage = "output.png"

    encryptedFile = ""
    if encrypted == True:
        shutil.copyfile(file, "enc")
        encryptedFile = "enc"
        crypt.encryptFile(file, encryptedFile)
        file = encryptedFile

    fileBinary = util.fileToBinary(file)

    if encrypted == True:
        os.remove(encryptedFile)

    stegoImage = pvd.PVDHide(keyImage, fileBinary)

    stegoImage = main.setMark(stegoImage, 1)

    util.imageFromArray(stegoImage).save(outputImage)

    # jekyll
    stegoImage = util.loadImage("output.png")

    foundFileBinary = pvd.PVDFind(keyImage, stegoImage, keyImage - stegoImage)

    util.binaryToFile(foundFileBinary, "foundFile")

    if encrypted == True:
        crypt.decryptFile("foundFile")

    extension = util.addExtension("foundFile")

    originalFile = util.loadImage("images/towers_small.jpg")
    foundFile = util.loadImage("foundFile." + extension)
    os.remove("foundFile." + extension)
    os.remove(outputImage)
    assert(np.array_equal(originalFile, foundFile))


def test_EDGE_message_encrypted(randomString):
    encrypted = True

    originalMessage = randomString
    message = originalMessage

    if encrypted == True:
        message = crypt.encryptMessage(message)

    messageBinary = util.messageToBinary(message)

    maskImage = edge.makeMask(keyImage)
    stegoImage = edge.EDGEHide(keyImage, maskImage, messageBinary)

    stegoImage = main.setMark(stegoImage, 0)

    util.computeStatistics(keyImage, stegoImage)

    util.imageFromArray(stegoImage).save("output.png")

    # jekyll
    stegoImage = util.loadImage("output.png")
    foundMessageBinary = edge.EDGEFind(keyImage, stegoImage, maskImage)

    foundMessage = util.binaryToMessage(foundMessageBinary)

    if encrypted == True:
        foundMessage = crypt.decryptMessage(foundMessage)

    os.remove("output.png")

    assert(foundMessage == originalMessage)

def test_EDGE_message(randomString):
    encrypted = False

    originalMessage = randomString
    message = originalMessage

    if encrypted == True:
        message = crypt.encryptMessage(message)

    messageBinary = util.messageToBinary(message)

    maskImage = edge.makeMask(keyImage)
    stegoImage = edge.EDGEHide(keyImage, maskImage, messageBinary)

    stegoImage = main.setMark(stegoImage, 0)

    util.computeStatistics(keyImage, stegoImage)

    util.imageFromArray(stegoImage).save("output.png")

    # jekyll
    stegoImage = util.loadImage("output.png")
    foundMessageBinary = edge.EDGEFind(keyImage, stegoImage, maskImage)

    foundMessage = util.binaryToMessage(foundMessageBinary)

    if encrypted == True:
        foundMessage = crypt.decryptMessage(foundMessage)

    os.remove("output.png")

    assert(foundMessage == originalMessage)

def test_EDGE_file_encrypted():
    encrypted = True
    file = "images/towers_extraSmall.jpg"
    outputImage = "output.png"

    encryptedFile = ""
    if encrypted == True:
        shutil.copyfile(file, "enc")
        encryptedFile = "enc"
        crypt.encryptFile(file, encryptedFile)
        file = encryptedFile

    fileBinary = util.fileToBinary(file)

    if encrypted == True:
        os.remove(encryptedFile)

    if edge.EDGEcheckCapacity(keyImage, fileBinary) == False:
        return

    maskImage = edge.makeMask(keyImage)

    stegoImage = edge.EDGEHide(keyImage, maskImage, fileBinary)

    stegoImage = main.setMark(stegoImage, 1)

    util.imageFromArray(stegoImage).save(outputImage)

    # jekyll
    stegoImage = util.loadImage("output.png")

    foundFileBinary = edge.EDGEFind(keyImage, stegoImage, maskImage)

    util.binaryToFile(foundFileBinary, "foundFile")

    if encrypted == True:
        crypt.decryptFile("foundFile")

    extension = util.addExtension("foundFile")

    originalFile = util.loadImage("images/towers_extraSmall.jpg")
    foundFile = util.loadImage("foundFile." + extension)
    os.remove("foundFile." + extension)
    os.remove(outputImage)
    assert(np.array_equal(originalFile, foundFile))

def test_EDGE_file():
    encrypted = False
    file = "images/towers_extraSmall.jpg"
    outputImage = "output.png"

    encryptedFile = ""
    if encrypted == True:
        shutil.copyfile(file, "enc")
        encryptedFile = "enc"
        crypt.encryptFile(file, encryptedFile)
        file = encryptedFile

    fileBinary = util.fileToBinary(file)

    if encrypted == True:
        os.remove(encryptedFile)

    if edge.EDGEcheckCapacity(keyImage, fileBinary) == False:
        return

    maskImage = edge.makeMask(keyImage)

    stegoImage = edge.EDGEHide(keyImage, maskImage, fileBinary)

    stegoImage = main.setMark(stegoImage, 1)

    util.imageFromArray(stegoImage).save(outputImage)

    # jekyll
    stegoImage = util.loadImage("output.png")

    foundFileBinary = edge.EDGEFind(keyImage, stegoImage, maskImage)

    util.binaryToFile(foundFileBinary, "foundFile")

    if encrypted == True:
        crypt.decryptFile("foundFile")

    extension = util.addExtension("foundFile")

    originalFile = util.loadImage("images/towers_extraSmall.jpg")
    foundFile = util.loadImage("foundFile." + extension)
    os.remove("foundFile." + extension)
    os.remove(outputImage)
    assert(np.array_equal(originalFile, foundFile))
