from setuptools import setup

setup(
    name = "jekyll",
    version = "1.0",
    packages = ["jekyll"],
    entry_points = """
    [console_scripts]
    hyde=jekyll.__main__:hyde
    jekyll=jekyll.__main__:jekyll
    """
)